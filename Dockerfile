FROM python:alpine

RUN set -ex; \
    apk update; \
    apk add --no-cache --virtual build-deps gcc libuv musl-dev make; \
    pip --no-cache-dir install sanic; \
    apk del build-deps

COPY flisolcounter/app.py /

EXPOSE 8000

CMD [ "python", "app.py"]
