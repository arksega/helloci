from flisolcounter.app import app as application
import pytest


@pytest.yield_fixture
def app():
    yield application


@pytest.fixture
def sanic_server(loop, app, sanic_client):
    return loop.run_until_complete(sanic_client(app))


async def test_index(sanic_server):
    resp = await sanic_server.get('/')
    assert resp.status == 200


async def test_greetingis(sanic_server):
    resp = await sanic_server.get('/greetings')
    assert resp.status == 200
